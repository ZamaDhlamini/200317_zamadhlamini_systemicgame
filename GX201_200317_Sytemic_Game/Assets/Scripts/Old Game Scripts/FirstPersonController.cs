﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
   
   public Transform camera;
   public Rigidbody rb;
   public float camRotaionSpeed = 5f;
   public float cameraMinimumY = -60f;
   public float cameraMaximumY = 75f;
   public float rotationSmoothSpeed = 10f;
   public float walkSpeed = 9f;
   public float runSpeed = 14f;
   public float maxSpeed = 20f;
   
   float bodyRotationX;
   float camRotationY;
   Vector3 directionIntentX;
   Vector3 directionIntentY;
   float speed;

    
    void Update()
    {
        LookRotation();
        Movement();
    }
    

 void LookRotation(){
    // Getting camera and body rotational values
    bodyRotationX += Input.GetAxis("Mouse X") * camRotaionSpeed;
    camRotationY += Input.GetAxis("Mouse Y") * camRotaionSpeed;

    //Disables camera from rotating 360 degrees
    camRotationY = Mathf.Clamp(camRotationY, cameraMinimumY, cameraMaximumY);

    //Making rotation targets and controls rotations of the body and the camera
    Quaternion camTargetRotation = Quaternion.Euler(-camRotationY, 0, 0);
    //for the body 
    Quaternion bodyTargetRotation = Quaternion.Euler(0, bodyRotationX, 0);

    //handles the rotation
    transform.rotation = Quaternion.Lerp(transform.rotation, bodyTargetRotation, Time.deltaTime * rotationSmoothSpeed);
    camera.localRotation = Quaternion.Lerp(camera.localRotation, camTargetRotation, Time.deltaTime * rotationSmoothSpeed);
 }

 void Movement(){
    //the player direction must match camera direction
    directionIntentX = camera.right;
    directionIntentX.y = 0;
    directionIntentX.Normalize();

    directionIntentY = camera.forward;
    directionIntentY.y = 0;
    directionIntentY.Normalize();

    //Characters velocity change in this direction
    rb.velocity = directionIntentY * Input.GetAxis("Vertical") * speed + directionIntentX * Input.GetAxis("Horizontal") * speed + Vector3.up * rb.velocity.y;
    rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);

    //Control speed based on movement state
    if(Input.GetKey(KeyCode.LeftShift)){
       speed = runSpeed;
    }
    if(!Input.GetKey(KeyCode.LeftShift)){
       speed = walkSpeed;
    }

 }
}
