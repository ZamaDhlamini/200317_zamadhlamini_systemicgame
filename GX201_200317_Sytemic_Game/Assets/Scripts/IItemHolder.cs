﻿
public interface IItemHolder 
{
   bool ContainsItems(Items item);
   int ItemCount(Items item);
   bool RemoveItem(Items item);
   bool AddItem(Items item);
   bool IsFull();
}
