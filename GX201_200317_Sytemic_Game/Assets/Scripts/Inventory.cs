﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour, IItemHolder
{
   [SerializeField] List<Items> items;
   [SerializeField] Transform itemsParent;
   [SerializeField] ItemSlot[] itemSlots;
   public event Action<Items> OnItemRightClickedEvent;

   private void Start() {
       for (int i = 0; i < itemSlots.Length; i++)
       {
           itemSlots[i].OnRightClickEvent += OnItemRightClickedEvent;
       }
   }

   private void OnValidate() 
   {
       if (itemsParent != null)
           itemSlots = itemsParent.GetComponentsInChildren<ItemSlot>();

       RefreshUI();
   }

   private void RefreshUI()
   {
       int i = 0;
       //for every item i have we will assign it to item slots
       for (; i < items.Count && i < itemSlots.Length; i++)
       {
            itemSlots[i].Items = items[i];
       }
       
       // remaning slots that doesnt have an item will be null
       for (; i < itemSlots.Length; i++)
       {
           itemSlots[i].Items = null;
       }
   }

   public bool AddItem(Items item)
   {
       if(IsFull())
          return false;

       items.Add(item);
       RefreshUI();
       return true;
   }

   public bool RemoveItem(Items item)
   {
       if(items.Remove(item)){
           RefreshUI();
           return true;
       }
       return false;
   }

   public bool IsFull()
   {
       return items.Count >= itemSlots.Length;
   }

    public bool ContainsItems(Items item)
    {
         if(items.Remove(item)){
           RefreshUI();
           return true;
       }
       return false;
    }

    public int ItemCount(Items item)
    {
        int number = 0;
          if(items.Remove(item)){
           RefreshUI();
           
           number++;
       }
       return number;
    }
}
