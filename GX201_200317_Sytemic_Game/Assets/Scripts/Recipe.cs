﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public struct ItemAmount{
    public Items Items;
    [Range(1, 999)]
    public int Amount;
}
[CreateAssetMenu]
public class Recipe : ScriptableObject
{
    public List<ItemAmount> Ingredients;
    public List<ItemAmount> Outcome;

//checks if inventory contains all the ingredients and correct amounts
    public bool CanCraft(IItemHolder itemHolder)
    {
        foreach (ItemAmount itemAmount in Ingredients)
        {
           if (itemHolder.ItemCount(itemAmount.Items) < itemAmount.Amount)
           {
               return false;
           }
        }
        return true;
    }

    public void Craft(IItemHolder itemHolder)
    {
       if (CanCraft(itemHolder))
       {
           foreach (ItemAmount itemAmount in Ingredients)
           {
               for (int i = 0; i < itemAmount.Amount; i++)
               {
                   itemHolder.RemoveItem(itemAmount.Items);
                   
               }
           }
            foreach (ItemAmount itemAmount in Outcome)
           {
               for (int i = 0; i < itemAmount.Amount; i++)
               {
                   itemHolder.AddItem(itemAmount.Items);
                   
               }
           }
       }
    }

}
