﻿using System;
using UnityEngine;

public class EquipmentPanel : MonoBehaviour
{
   [SerializeField] Transform equipmentSlotsParent;
   [SerializeField] EquipmentSlot[] equipmentSlots;

   public event Action<Items> OnItemRightClickedEvent;

   private void Start() {
       for (int i = 0; i < equipmentSlots.Length; i++)
       {
           equipmentSlots[i].OnRightClickEvent += OnItemRightClickedEvent;
       }
   }

   private void OnValidate() {
       equipmentSlots = equipmentSlotsParent.GetComponentsInChildren<EquipmentSlot>();
   }

   public bool AddItem(EquippableItem item, out EquippableItem previousItem)
   {
       for (int i = 0; i < equipmentSlots.Length; i++)
       {
           if(equipmentSlots[i].IngredientType == item.IngredientType)
           {
               previousItem = (EquippableItem)equipmentSlots[i].Items;
               equipmentSlots[i].Items = item;
               return true;
           }
       }
       previousItem = null;
       return false;
   }
   public bool RemoveItem(EquippableItem item)
   {
       for (int i = 0; i < equipmentSlots.Length; i++)
       {
           if(equipmentSlots[i].Items == item)
           {
               equipmentSlots[i].Items = null;
               return true;
           }
       }
       return false;
   }

}
