﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


[Serializable]
public class CharacterStat 
{
  public float BaseValue;
  public virtual float Value{
      get{
          if (isDirty || BaseValue != lastBaseVAlue){
              lastBaseVAlue = BaseValue;
              _value = CalculateFinalValue();
              isDirty = false;
          }
          return _value;
      }
  }    
  //indicate whether we need to recalculate the value
  protected bool isDirty = true;
  //holds most recent calculation that was done
  protected float _value;
  protected float lastBaseVAlue = float.MinValue;
  //list of stat modifiers that will hold all the stat modifiers that have been applied to the stat
  protected readonly List<StatModifier> statModifiers;
  public readonly ReadOnlyCollection<StatModifier> StatsModifiers;

  public CharacterStat(){
      statModifiers = new List<StatModifier>();
      StatsModifiers = statModifiers.AsReadOnly();
  }

  public CharacterStat(float baseValue) : this()
  {
      BaseValue = baseValue;
      
  }

  public virtual void AddModifier(StatModifier mod){
      isDirty = true;
      statModifiers.Add(mod);
      statModifiers.Sort(CompareModifierOrder);
  }

  protected virtual int CompareModifierOrder(StatModifier a, StatModifier b){
      if (a.Order < b.Order)
          return -1;
      else if (a.Order > b.Order)
           return 1;
      return 0; 
  }

  public virtual bool RemoveModifier(StatModifier mod){
      
      if (statModifiers.Remove(mod)){
          isDirty = true;
          return true;
      }
      return false;
  }

  public virtual bool RemoveAllModifiersFromSource(object source)
  {
      bool didRemove = false;
      for (int i = statModifiers.Count - 1; i >= 0; i--)
      {
          if(statModifiers[i].Source == source)
          {
              isDirty = true;
              didRemove = true;
              statModifiers.RemoveAt(i);
          }
      }
      return didRemove;
  }

  protected virtual float CalculateFinalValue(){
      float finalValue = BaseValue;
      float sumPercentAdd = 0;
      for (int i = 0; i < statModifiers.Count; i++)
      {
          StatModifier mod = statModifiers[i];

          if(mod.Type == StatModType.Flat)
          {
          finalValue += mod.Value;

          }
          else if (mod.Type == StatModType.PercentAdd)
          {
              sumPercentAdd += mod.Value;

              if(i + 1 >= statModifiers.Count || statModifiers[i + i].Type != StatModType.PercentAdd)
              {
                 finalValue *= 1 + sumPercentAdd;
                 sumPercentAdd = 0;
              }
          }
          else if (mod. Type == StatModType.PercentMult)
          {
              finalValue *= 1 + mod.Value;
          }
      }
      //Rounding up to four digits
      return(float)Math.Round(finalValue, 4);
  }
}
