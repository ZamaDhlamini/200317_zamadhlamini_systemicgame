﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentSlot : ItemSlot
{
   public IngredientType IngredientType;

   protected override void OnValidate() {
       base.OnValidate();
       gameObject.name = IngredientType.ToString() + "Slot";
   }
}
