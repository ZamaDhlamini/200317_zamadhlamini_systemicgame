﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ItemSlot : MonoBehaviour, IPointerClickHandler
{
   [SerializeField] Image Image;
   public event Action<Items> OnRightClickEvent;
   //making this variable into a property
   private Items _items;
   public Items Items {
       get { return _items; }
       set {
           _items = value;
           if(_items == null){
               Image.enabled = false;
           } else{
               Image.sprite = _items.Icon;
               Image.enabled = true;
           }
       }
   }
    public void OnPointerClick(PointerEventData eventData)
    {
        if(eventData != null && eventData.button == PointerEventData.InputButton. Right)
        {
            if(Items != null && OnRightClickEvent != null)
               OnRightClickEvent(Items);
        }
    }

   protected virtual void OnValidate() 
   {
       //if the image component is null it will try to find the component
       if (Image == null)
       {
           Image = GetComponent<Image>();
       }
   }

}
