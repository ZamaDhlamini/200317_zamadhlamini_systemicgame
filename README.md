# Systemic Game: Cooking and Survival SandBox Game

This term we were tasked at making a sandbox systemic game. Our main focus was to gain experience with data management, inter-system communication, and project architecture. In my sandbox game I wanted to achieve the system of having the ability to equip certain ingredients that can effect the players health and ability. These two systems are supposed to work with eachother, so when one is intereacted another result comes from that. Furthermore, the crafting system was meant to be there so that the player was able to craft based off of the inventory ingredients, which inturn affets the stats by what the player creates.

## Features
1. Can interact with the items in your inventory by right clicking
2. By doing that it can add the items to the player, by equipping.
3. Then can see what certain Ingredients and food does to your health
4. Can see different recipes to see what can be made
5. Be able to make certain dishes out of those recipes that can then affect your stats as well.



